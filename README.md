# Megaman Legends 1

Save State Viewer for Megaman Legends 1 on the PSX. The reason this exists is because
the Playstation doesn't use a simple mechanism for matching textures to meshes, such
as a texture id or a texture name. The Playstation has 1MB of Vram, which palettes and
images are loaded into, and the game loads an image/palette combinations when mapping
an image onto a face.  

While it is possible to load and export textures individually, there's no sure-fire way
of defining which tectures get matched with what meshes, as it entirely depends on what
files are loaded into a given scene, which is something that hasn't been data-mined.
The other option is to manually pair all of the textures (something that was done in a
prior version for a subset of assets), or the option that this tool uses, which is to 
use a save state where the relationship between the assets and the textures are managed
by the original game, and we are able to take advantage of it.

**How does the tool work?**

The Playstation has a few main components, a disk drive, 1MB of Vram, and 2MB of
memory. When the Playstation loads a scene, it reads asset data from the disk to
load into the 2MB of memory, and texture data to load into vram. The Playstation
is able to draw data directly through the screen through a set of API's, and thus
doesn't have a z-buffer where the assets are cached before drawing. The approach this
tool takes is to read the state of the assets (which is copied directly from the
original files), to read the geometry for the models, and then use the encoded
address for the vram to render the textures.

**How do I use the Tool?**
