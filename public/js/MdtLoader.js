"use strict";

const MdtLoader  = {

	setState : function(mem) {

		this.MEM = {};
		this.MEM.mem = mem;
		this.MEM.view = new DataView(mem.buffer);
		this.MEM.assets = {};
		
		const MDT_OFS = 0x00164000;

		let ofs = MDT_OFS;
		const count = this.MEM.view.getUint32(ofs, true);
		
		ofs += 4;
		const ul = document.createElement('ul');
		for(let i = 0; i < count; i++) {
	
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			let room = {
				x_dis : this.MEM.view.getInt8(ofs + 0),
				z_dis : this.MEM.view.getInt8(ofs + 1),
				x_len : this.MEM.view.getUint8(ofs + 2),
				z_len : this.MEM.view.getUint8(ofs + 3),
				ofs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff
			};
	
			ofs += 8;
			
			let name = "room_" + num;
			let li = document.createElement("li");
			li.setAttribute("data-type", "mdt");
			li.setAttribute("data-name", name);
			li.textContent = name;
			ul.appendChild(li);
			this.MEM.assets[name] = room;

		}
			
		let li = document.createElement("li");
		li.setAttribute("data-type", "mdt");
		li.setAttribute("data-name", "all");
		li.textContent = "Full Map";
		ul.appendChild(li);

		return ul;

	},

	fetchAsset : function(name) {
		
		switch(name) {
		case "all":

			let list = [];
			for(let key in this.MEM.assets) {
				console.log(key);
				let mesh = this.renderRoom(key);
				list.push(mesh);
			}
			Viewport.API.setScene(list);

			break;
		default:
			let high = this.renderRoom(name, "high");
			let med = this.renderRoom(name, "med");
			let low = this.renderRoom(name, "low");
			Viewport.API.setAsset([high, med, low]);
			break;
		} 

	},

	renderRoom : function (name, lod, mats) {

		const room = this.MEM.assets[name];
		let ofs = room.ofs;

		const floor = [];
		const flat = [];
		const terrain = [];

		const unique = [];

		for(let z = 0; z < room.z_len; z++) {
			for(let x = 0; x < room.x_len; x++) {

				let type = this.MEM.view.getUint16(ofs, true);
				ofs += 2;

				if(type === 0) {
					continue;
				}

				if(type & 0x8000) {
					
					if(!floor.length) {
						console.log("First floor tile at: 0x%s", (ofs-2).toString(16));
					}
					let y = (type >> 8) & 0x0f;

					floor.push({
						x_dis : x,
						y_dis : y,
						z_dis : z,
						type : type
					});
					
					type = Math.floor((type & 0xff) / 4);
					let hex = type.toString(16);
					if(unique.indexOf(hex) === -1) {
						unique.push(hex);
					}

					let index = type & 0xff;
					continue;
				}

				if(terrain.indexOf(type) !== -1) {
					continue;
				}

				terrain.push(type);

			}
		}

		floor.shift();

		let tiles = [];
		const IDX_OFS = 0x0015c000;
		const STG_OFS = 0x00194000;
		
		const STAGE = [];

		floor.forEach(def => {
			
			let tile = {};
			let matrix = new THREE.Matrix4();

			const room_x = (room.x_dis-63) * 3.2;
			const room_z = (room.z_dis-63) * -3.2;
			matrix.setPosition(
				room_x + (def.x_dis - 1) * 3.2 + 1.6, 
				def.y_dis * 0.8,
				room_z + (def.z_dis) * -3.2 + 1.6
			);

			STAGE.push({
				type : 'floor',
				index : def.type & 0xff,
				x : room_x + (def.x_dis - 1) * 3.2 + 1.6, 
				y : def.y_dis * 0.8,
				z : room_z + (def.z_dis) * -3.2 + 1.6
			})

			tile.matrix = matrix;
			tile.high_ofs = this.MEM.view.getUint32(STG_OFS + 0x24, true) & 0xffffff;
			tile.med_ofs = this.MEM.view.getUint32(STG_OFS + 0x28, true) & 0xffffff;
			tile.low_ofs = this.MEM.view.getUint32(STG_OFS + 0x2c, true) & 0xffffff;
			tiles.push(tile);

		});

		terrain.forEach(index => {

			ofs = IDX_OFS + (index * 0x0c);
			let tile = {
				a : this.MEM.view.getInt8(ofs + 0),
				loc_x : this.MEM.view.getInt8(ofs + 1) * 1.6,
				c : this.MEM.view.getInt8(ofs + 2),
				d : this.MEM.view.getInt8(ofs + 3) * -1.6,
				y : this.MEM.view.getInt16(ofs + 2),
				e : this.MEM.view.getInt8(ofs + 4),
				loc_z : this.MEM.view.getInt8(ofs + 5) * -1.6,
				g : this.MEM.view.getInt8(ofs + 6),
				h : this.MEM.view.getInt8(ofs + 7),
				stg_ofs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff
			};

			switch(tile.c) {
			case 0:
				break;
			case -128:
				tile.d -= 0.8;
				break;
			case -96:
				tile.d -= 1.0;
				break;
			case -64:
				tile.d -= 1.2;
				break;
			case -48:
				tile.d -= 1.25;
				break;
			default:
				console.log("new c value %d", tile.c);
				break;
			}

			STAGE.push({
				type : 'terrain',
				index : (tile.stg_ofs - STG_OFS) / 0x30,
				x : tile.loc_x,
				y : tile.d,
				z : tile.loc_z
			});

			let matrix = new THREE.Matrix4();
			matrix.setPosition(tile.loc_x, tile.d, tile.loc_z);

			tile.high_ofs = this.MEM.view.getUint32(tile.stg_ofs + 0x24, true) & 0xffffff;
			tile.med_ofs = this.MEM.view.getUint32(tile.stg_ofs + 0x28, true) & 0xffffff;
			tile.low_ofs = this.MEM.view.getUint32(tile.stg_ofs + 0x2c, true) & 0xffffff;

			tile.matrix = matrix;
			tiles.push(tile);

		});
	
		let materials = [];
		let geometry = new THREE.Geometry();
		geometry.userData = {}
		geometry.userData.tex = [];
		geometry.userData.mats = [];
		geometry.userData.imgs = [];
		geometry.userData.lookup = {};

		for(let i = 0; i < tiles.length; i++) {
			
			let ofs = tiles[i].high_ofs;
			switch(lod) {
			case "low":
				ofs = tiles[i].low_ofs;
				break;
			case "med":
				ofs = tiles[i].med_ofs;
				break;
			}
			
			
			StgLoader.renderTile(ofs, materials, geometry, tiles[i].matrix);
		}
		
		/*
		const debug_index = materials.length;
		const m = new THREE.MeshBasicMaterial({
			color : 0x000,
			side : THREE.DoubleSide
		});

		materials.push(m);
		const room_matrix = new THREE.Matrix4();
		room_matrix.setPosition(
			(room.x_dis-63) * 3.2,
			0,
			(room.z_dis-63) * -3.2
		);

		let debug_floor = floor;
		let lookup = geometry.userData.lookup;
		debug_floor.forEach(tile => {

			const v = [
				new THREE.Vector3(0, 0, 0),
				new THREE.Vector3(0, 0, -3.2),
				new THREE.Vector3(3.2, 0, 0),
				new THREE.Vector3(3.2, 0, -3.2)
			];
		
			console.log(tile.y_dis);

			let p = new Array(4);
			let mat4 = new THREE.Matrix4();
			mat4.setPosition(
				(tile.x_dis-1) * 3.2, 
				tile.y_dis * 0.8,
				(tile.z_dis-1) * -3.2
			);

			for(let i = 0; i < v.length; i++) {

				let vertex = v[i];

				vertex.applyMatrix4(room_matrix);
				vertex.applyMatrix4(mat4);

				let key = [
					vertex.x.toFixed(2),
					vertex.y.toFixed(2),
					vertex.z.toFixed(2)
				].join(",");
		
				if(!lookup.hasOwnProperty(key)) {
					lookup[key] = geometry.vertices.length;
					geometry.vertices.push(vertex);
				}

				p[i] = lookup[key];
			}

			let a = p[0];
			let b = p[1];
			let c = p[2];
			let d = p[3];
		
			let face_a = new THREE.Face3(b, a, c);
			let face_b = new THREE.Face3(d, b, c);

			face_a.materialIndex = debug_index;
			face_b.materialIndex = debug_index;

			geometry.faces.push(face_a);
			geometry.faces.push(face_b);

			geometry.faceVertexUvs[0].push(new THREE.Vector2());
			geometry.faceVertexUvs[0].push(new THREE.Vector2());

		});
		*/

		geometry.computeFaceNormals();
		geometry.computeBoundingBox();

		let box = geometry.boundingBox;
		let mat = new THREE.MeshNormalMaterial();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);

		let mesh = new THREE.Mesh(buffer, materials);
		mesh.name = name;
		mesh.userData.faces = geometry.faces.length;
		mesh.userData.vertices = geometry.vertices.length;
		mesh.userData.lod = "High";
		mesh.userData.stage = STAGE;
		switch(lod) {
		case "low":
			mesh.userData.lod = "Low";
			break;
		case "med":
			mesh.userData.lod = "Medium";
			break;
		}
		return mesh;
	},

}
