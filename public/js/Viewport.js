/*
 * @author - Benjamin Collins (kion@dashgl.com)
 */

const Viewport = (function() {
	
	this.MEM = {
		list : [],
		activeMesh : [],
		clock : new THREE.Clock(),
		scene : new THREE.Scene(),
		renderer : new THREE.WebGLRenderer({
			antialias: true,
			alpha : true,
			preserveDrawingBuffer: true
		}),
		camera : new THREE.PerspectiveCamera(),
		options : {
			grid : true,
			box : true,
			skel : true,
			info : true
		},
		mixers : [],
		speed : 1
	}

	this.DOM = {
		section : document.getElementById('Viewport.section'),
		menu : {
			state : document.getElementById('Viewport.menu.state'),
			download : document.getElementById('Viewport.menu.download'),
			settings : document.getElementById('Viewport.menu.setting'),
			gitlab : document.getElementById('Viewport.menu.gitlab')
		},
		modal : {
			body : document.getElementById('Viewport.modal.body'),
			grid : document.getElementById('Viewport.modal.grid'),
			box : document.getElementById('Viewport.modal.box'),
			skel : document.getElementById('Viewport.modal.skel'),
			info : document.getElementById('Viewport.modal.info'),
			center : document.getElementById('Viewport.modal.center')
		},
		preview : {
			prev : document.getElementById('Viewport.preview.prev'),
			next : document.getElementById('Viewport.preview.next'),
			info : document.getElementById('Viewport.preview.info')
		},
		anims : {
			select : document.getElementById('Viewport.anims.select'),
			play : document.getElementById('Viewport.anims.play'),
			pause : document.getElementById('Viewport.anims.pause'),
			stop : document.getElementById('Viewport.anims.stop'),
			prev : document.getElementById('Viewport.anims.prev'),
			next : document.getElementById('Viewport.anims.next'),
			frame : document.getElementById('Viewport.anims.frame'),
			up : document.getElementById('Viewport.anims.up'),
			down : document.getElementById('Viewport.anims.down'),
			speed : document.getElementById('Viewport.anims.speed')
		}
	}

	this.EVT = {
		handleWindowResize : evt_handleWindowResize.bind(this),
		handleStateClick : evt_handleStateClick.bind(this),
		handleDownloadClick : evt_handleDownloadClick.bind(this),
		handleSettingsClick : evt_handleSettingsClick.bind(this),
		handleGitlabClick : evt_handleGitlabClick.bind(this),
		handleDocumentClick : evt_handleDocumentClick.bind(this),
		handleSettingsMenu : evt_handleSettingsMenu.bind(this),
		handleSwitchClick : evt_handleSwitchClick.bind(this),
		handleAnimSelect : evt_handleAnimSelect.bind(this),
		handlePlayClick : evt_handlePlayClick.bind(this),
		handlePauseClick : evt_handlePauseClick.bind(this),
		handleStopClick : evt_handleStopClick.bind(this),
		handleNextClick : evt_handleNextClick.bind(this),
		handlePrevClick : evt_handlePrevClick.bind(this),
		handleSpeedUp : evt_handleSpeedUp.bind(this),
		handleSpeedDown : evt_handleSpeedDown.bind(this)
	}

	this.API = {
		animate : api_animate.bind(this),
		setViewport : api_setViewport.bind(this),
		setScene : api_setScene.bind(this),
		setMesh : api_setMesh.bind(this),
		setAsset : api_setAsset.bind(this),
		exportGLTF : api_exportGLTF.bind(this)
	};

	init.apply(this);
	return this;

	function init() {
		
		let str = localStorage.getItem("motb");
		if(str) {
			this.MEM.options = JSON.parse(str);
		}

		for(let key in this.MEM.options) {
			if(this.MEM.options[key]) {
				this.DOM.modal[key].classList.add("on");
			} else {
				this.DOM.modal[key].classList.remove("on");
			}
		}

		this.MEM.camera.position.z = 25;
		this.MEM.camera.position.y = 10;
		this.MEM.camera.position.x = 0;
		this.MEM.camera.lookAt(new THREE.Vector3(0, 10, 0));

		this.MEM.renderer.domElement.style.margin = "0";
		this.MEM.renderer.domElement.style.padding = "0";
		this.MEM.renderer.setClearColor(0x000000, 0);

		this.MEM.grid = new THREE.GridHelper(50, 20, 0xff0000, 0xffffff);
		this.MEM.grid.visible = this.MEM.options.grid;

		this.MEM.scene.add(this.MEM.grid);

		this.DOM.section.appendChild(this.MEM.renderer.domElement);
		THREE.OrbitControls(this.MEM.camera, this.MEM.renderer.domElement);
		
		window.addEventListener("resize", this.EVT.handleWindowResize);
		this.API.setViewport();
		this.API.animate();

		this.DOM.menu.state.addEventListener('click', this.EVT.handleStateClick);
		this.DOM.menu.download.addEventListener('click', this.EVT.handleDownloadClick);
		this.DOM.menu.settings.addEventListener('click', this.EVT.handleSettingsClick);
		this.DOM.menu.gitlab.addEventListener('click', this.EVT.handleGitlabClick);
		document.body.addEventListener('click', this.EVT.handleDocumentClick);
		this.DOM.modal.body.addEventListener('click', this.EVT.handleSettingsMenu);

		this.DOM.preview.next.addEventListener('click', this.EVT.handleSwitchClick);
		this.DOM.preview.prev.addEventListener('click', this.EVT.handleSwitchClick);

		this.DOM.anims.select.addEventListener('change', this.EVT.handleAnimSelect);
		this.DOM.anims.play.addEventListener('click', this.EVT.handlePlayClick);
		this.DOM.anims.pause.addEventListener('click', this.EVT.handlePauseClick);
		this.DOM.anims.stop.addEventListener('click', this.EVT.handleStopClick);
		this.DOM.anims.prev.addEventListener('click', this.EVT.handlePrevClick);
		this.DOM.anims.next.addEventListener('click', this.EVT.handleNextClick);
		this.DOM.anims.up.addEventListener('click', this.EVT.handleSpeedUp);
		this.DOM.anims.down.addEventListener('click', this.EVT.handleSpeedDown);
		
		this.DOM.anims.speed.textContent = this.MEM.speed.toFixed(1);

	}

	function evt_handleWindowResize() {

		this.API.setViewport();

	}

	function evt_handleStateClick() {

		Memory.DOM.input.click();

	}

	async function evt_handleDownloadClick() {

		let list = this.MEM.list || [];
		if(!list.length) {
			return;
		}

		const zip = new JSZip();

		for(let i = 0; i < list.length; i++) {
			
			let mesh = list[i];

			let glb = await this.API.exportGLTF(mesh);
			zip.file(mesh.name + ".glb", glb);

			if(mesh.userData.stage) {
				let text = JSON.stringify(mesh.userData.stage, null, 4);
				zip.file(mesh.name + ".json", text);
			}

			let dashExporter = new THREE.DashExporter();
			let dmf = dashExporter.parse(mesh);
			zip.file(mesh.name + ".dmf", dmf);

		}

		let blob = await zip.generateAsync({type:"blob"});
		saveAs(blob, this.MEM.asset_name + ".zip");

	}

	function evt_handleSettingsClick(evt) {

		if(this.DOM.modal.body.classList.contains("hide")) {
			this.DOM.modal.body.classList.remove("hide");
			evt.stopPropagation();
		}
		
	}

	function evt_handleGitlabClick() {

		const URL = "https://gitlab.com/megamanlegends/motb";
		window.open(URL, '_blank');

	}

	function evt_handleDocumentClick(evt) {

		if(this.DOM.modal.body.classList.contains("hide")) {
			return;
		}
		
		if(this.DOM.modal.body.contains(evt.target)) {
			return;
		}
		
		this.DOM.modal.body.classList.add("hide");

	}

	function evt_handleSettingsMenu(evt) {

		let li = evt.target;
		if(li.tagName !== "LI") {
			return;
		}

		let leaf = li.getAttribute("id").split(".").pop();

		switch(leaf) {
		case "grid":
		case "box":
		case "skel":
		case "info":

			li.classList.toggle("on");
			this.MEM.options[leaf] = li.classList.contains("on");
			let str = JSON.stringify(this.MEM.options);
			localStorage.setItem("motb", str);

			break;
		case "camera":

			this.MEM.saveImage = true;

			break;
		case "center":

			this.MEM.camera.position.z = 55;
			this.MEM.camera.position.y = 25;
			this.MEM.camera.position.x = 0;
			this.MEM.camera.lookAt(new THREE.Vector3(0, 0, 0));

			break;
		}

		this.MEM.grid.visible = this.MEM.options.grid;
		if(this.MEM.box) {
			this.MEM.box.visible = this.MEM.options.box;
		}

		if(this.MEM.helper) {
			this.MEM.helper.visible = this.MEM.options.skel;
		}

		if(this.MEM.options.info && this.MEM.activeMesh) {
			this.DOM.preview.info.classList.remove("hide");
		} else if(!this.MEM.options.info) {
			this.DOM.preview.info.classList.add("hide");
		}

	}

	function evt_handleSwitchClick(evt) {

		let index = evt.target.getAttribute("data-index");
		if(!index) {
			return;
		}

		index = parseInt(index);
		this.API.setMesh(index);

	}

	function evt_handleAnimSelect() {

		let index = parseInt(this.DOM.anims.select.value);
		
		if(this.MEM.action) {
			this.MEM.action.stop();
			this.MEM.action = null;
		}

		this.DOM.anims.frame.value = 0;

		if(index === -1) {
			return;
		}

		let mesh = this.MEM.activeMesh[0];
		let mixer = new THREE.AnimationMixer(mesh);
		let clip = mesh.geometry.animations[index];
		let action = mixer.clipAction(clip, mesh);
		action.timeScale = this.MEM.speed;
		this.MEM.action = action;
		action.play();
		this.MEM.mixers = [mixer];

		this.DOM.anims.frame.setAttribute("min", "0");
		this.DOM.anims.frame.setAttribute("max", parseInt(clip.duration * 100));

	}

	function evt_handlePlayClick() {

		if(!this.MEM.action) {
			return;
		}

		this.MEM.action.play();
		this.MEM.action.paused = 0;

	}

	function evt_handlePauseClick() {

		if(!this.MEM.action) {
			return;
		}

		this.MEM.action.paused = 1;
		
	}

	function evt_handleStopClick() {

		if(!this.MEM.action) {
			return;
		}

		this.MEM.action.stop();

	}

	function evt_handlePrevClick() {

		let index = this.DOM.anims.select.selectedIndex;
		if(index > 0) {
			index--;
		}
		this.DOM.anims.select.selectedIndex = index;
		this.EVT.handleAnimSelect();

	}

	function evt_handleNextClick() {

		let index = this.DOM.anims.select.selectedIndex;
		if(index < this.DOM.anims.select.length - 1) {
			index++;
		}
		this.DOM.anims.select.selectedIndex = index;
		this.EVT.handleAnimSelect();

	}

	function evt_handleSpeedUp() {

		if(this.MEM.speed >= 2.0) {
			return;
		}

		this.MEM.speed += 0.1;
		this.DOM.anims.speed.textContent = this.MEM.speed.toFixed(1);

		if(this.MEM.action) {
			this.MEM.action.timeScale = this.MEM.speed;
		}

	}

	function evt_handleSpeedDown() {

		if(this.MEM.speed <= -2.0) {
			return;
		}

		this.MEM.speed -= 0.1;
		this.DOM.anims.speed.textContent = this.MEM.speed.toFixed(1);

		if(this.MEM.action) {
			this.MEM.action.timeScale = this.MEM.speed;
		}

	}


	function api_animate() {

		requestAnimationFrame(this.API.animate);
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);

		let delta = this.MEM.clock.getDelta();
		this.MEM.mixers.forEach(mixer => {
			mixer.update(delta);
			if(this.MEM.action) {
				this.DOM.anims.frame.value = parseInt(this.MEM.action.time * 100);
			}
		});

		if(this.MEM.saveImage) {
			this.MEM.saveImage = false;
			this.MEM.renderer.domElement.toBlob(function (blob) {
				saveAs(blob, Date.now() + ".png");
			});
		}

	}

	function api_setViewport() {

		let width = this.DOM.section.offsetWidth;
		let height = this.DOM.section.offsetHeight;

		this.MEM.camera.aspect = width / height;
		this.MEM.camera.updateProjectionMatrix();
		this.MEM.renderer.setSize( width, height);

	}

	function api_setMesh(index) {

		while(this.MEM.activeMesh.length) {
			this.MEM.scene.remove(this.MEM.activeMesh.pop());
		}

		this.MEM.mixers = [];
		this.DOM.anims.frame.value = 0;
		this.MEM.action = null;

		let mesh = this.MEM.list[index];
		this.MEM.scene.add(mesh);
		this.MEM.activeMesh = [ mesh ];
		
		this.MEM.box = new THREE.BoxHelper( mesh, 0xffff00 );
		this.MEM.box.visible = this.MEM.options.box;
		mesh.add(this.MEM.box);

		if(mesh.skeleton) {
			this.MEM.helper = new THREE.SkeletonHelper(mesh);
			this.MEM.helper.material.linewidth = 2;
			this.MEM.helper.visible = this.MEM.options.skel;
			mesh.add(this.MEM.helper);
			//this.MEM.scene.add(this.MEM.helper);
		}

		if(index === 0) {
			this.DOM.preview.prev.classList.add("hide");
		} else {
			this.DOM.preview.prev.classList.remove("hide");
			this.DOM.preview.prev.setAttribute("data-index", index - 1);
		}

		if(index < this.MEM.list.length - 1) {
			this.DOM.preview.next.classList.remove("hide");
			this.DOM.preview.next.setAttribute("data-index", index + 1);
		} else {
			this.DOM.preview.next.classList.add("hide");
		}

		if(this.MEM.options.info) {
			this.DOM.preview.info.classList.remove("hide");
		}

		this.DOM.preview.info.innerHTML = "";
		let tri = document.createElement("li");
		let vec = document.createElement("li");
		let mat = document.createElement("li");
		let bns = document.createElement("li");
		let lod = document.createElement("li");

		tri.textContent = "Number of triangles : " + mesh.userData.faces;
		vec.textContent = "Number of vertices : " + mesh.userData.vertices;
		mat.textContent = "Number of materials : " + mesh.material.length;
		bns.textContent = "Number of bones : " + (mesh.skeleton ? mesh.skeleton.bones.length : 0);
		lod.textContent = "Level of Detail : " + mesh.userData.lod;

		this.DOM.preview.info.appendChild(tri);
		this.DOM.preview.info.appendChild(vec);
		this.DOM.preview.info.appendChild(mat);
		this.DOM.preview.info.appendChild(bns);
		this.DOM.preview.info.appendChild(lod);

		this.DOM.anims.select.innerHTML = "<option value='-1'>Choose an Animation</option>";

		if(mesh.geometry.animations) {
			
			for(let i = 0; i < mesh.geometry.animations.length; i++) {
				let option = document.createElement("option");
				option.setAttribute("value", i);
				option.textContent = mesh.geometry.animations[i].name;
				this.DOM.anims.select.appendChild(option);
			}

		}

	}

	function api_setAsset(list) {
		
		this.MEM.list = list;
		this.API.setMesh(0);

	}

	function api_setScene(list) {
		
		this.DOM.preview.prev.classList.add("hide");
		this.DOM.preview.next.classList.add("hide");

		while(this.MEM.activeMesh.length) {
			this.MEM.scene.remove(this.MEM.activeMesh.pop());
		}

		this.MEM.list = list;
		this.MEM.activeMesh = list;

		let f = 0;
		let v = 0;
		let m = 0;

		list.forEach(mesh => {

			if(!mesh.userData.helper) {
				this.MEM.box = new THREE.BoxHelper( mesh, 0xffff00 );
				this.MEM.box.visible = this.MEM.options.box;
				mesh.add(this.MEM.box);
			}

			f += mesh.userData.faces;
			v += mesh.userData.vertices;
			m += mesh.material.length;

			this.MEM.scene.add(mesh);
			
		});

		if(this.MEM.options.info) {
			this.DOM.preview.info.classList.remove("hide");
		}

		this.DOM.preview.info.innerHTML = "";
		let tri = document.createElement("li");
		let vec = document.createElement("li");
		let mat = document.createElement("li");
		let bns = document.createElement("li");
		let lod = document.createElement("li");

		tri.textContent = "Number of triangles : " + f;
		vec.textContent = "Number of vertices : " + v;
		mat.textContent = "Number of materials : " + m;
		bns.textContent = "Number of bones : 0"
		lod.textContent = "Level of Detail : High"

		this.DOM.preview.info.appendChild(tri);
		this.DOM.preview.info.appendChild(vec);
		this.DOM.preview.info.appendChild(mat);
		this.DOM.preview.info.appendChild(bns);
		this.DOM.preview.info.appendChild(lod);

	}

	function api_exportGLTF(mesh) {

		return new Promise( (resolve, reject) => {

            let anims = [];
            if(mesh.geometry) {
                anims = mesh.geometry.animations || []
            }

            let expt = new THREE.GLTFExporter();
            let opt = {
                binary : true,
                animations : anims
            };

            expt.parse(mesh, result => {

                let mime = {type:"application/octet-stream"};
                let blob = new Blob([result], mime);
                resolve(blob);

            }, opt);

		});

	}

}).apply({});
