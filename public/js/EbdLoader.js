
const EbdLoader = {

	setState : function(mem) {
		
		this.MEM = {};
		this.MEM.mem = mem;
		this.MEM.view = new DataView(mem.buffer);
		this.MEM.assets = {};

		const EBD_OFS = 0x16c000;
		const count = this.MEM.view.getUint32(EBD_OFS, true);
		const ul = document.createElement('ul');

		let ofs = EBD_OFS + 4;
		for(let i = 0; i < count; i++) {
			
			let asset = {
				id : this.MEM.view.getUint32(ofs + 0, true),
				mesh_ofs : this.MEM.view.getUint32(ofs + 0x04, true) & 0xffffff,
				pose_ofs : this.MEM.view.getUint32(ofs + 0x08, true) & 0xffffff,
				time_ofs : this.MEM.view.getUint32(ofs + 0x0c, true) & 0xffffff
			}
			
			let hex = asset.id.toString(16);
			if(hex.length % 2) {
				hex = "0" + hex;
			}

			this.MEM.assets[hex] = asset;
			let li = document.createElement('li');
			li.setAttribute("data-type", "ebd");
			li.setAttribute("data-name", hex);
			li.textContent = "em_" + hex;
			ul.appendChild(li);

			ofs += 0x10;

		}

		return ul;

	},

	fetchAsset : function(name) {
	
		let model = this.MEM.assets[name];

		let ofs = model.mesh_ofs + 0x10;
		let hierarchy_len = this.MEM.view.getUint8(ofs + 1);
		let hierarchy = new Array(hierarchy_len);
		
		for(let i = 0; i < hierarchy_len; i++) {

			let rel = {
				poly_id : this.MEM.view.getInt8(ofs + 0),
				parent_id : this.MEM.view.getInt8(ofs + 1),
				child_id : this.MEM.view.getUint8(ofs + 2),
				flags : this.MEM.view.getUint8(ofs + 3)
			}
			
			ofs += 4;
			hierarchy[i] = rel;

		}
		
		let bones = this.readBones(model, hierarchy, true);
		let lod = {
			High : this.MEM.view.getUint32(model.mesh_ofs + 0x70, true) & 0xffffff,
			Medium : this.MEM.view.getUint32(model.mesh_ofs + 0x74, true) & 0xffffff,
			Low : this.MEM.view.getUint32(model.mesh_ofs + 0x78, true) & 0xffffff
		};
		let animations = this.readAnimations(model, bones, true);

		let unique = [];
		let meshes = [];
		
		for(let key in lod) {

			if(unique.indexOf(lod[key]) !== -1) {
				continue;
			}
			
			unique.push(lod[key]);
			let polygons = this.readPolygons(lod[key], hierarchy);

			if(bones && bones.length < polygons.length) {

				let core = polygons.splice(0, bones.length);
			
				if(polygons[0].weight === 1) {
					core.push(polygons.shift());
				}

				let skel = this.readBones(model, hierarchy);
				let mesh = this.readGeometry(core, skel);
				mesh.name = name + "_" + key.toLowerCase();
				mesh.geometry.animations = animations;
				mesh.userData.lod = key;
				meshes.push(mesh);

				for(let i = 0; i < polygons.length; i++) {
					let part = this.readGeometry([polygons[i]], bones, true);
					part.name = name + "_" + key.toLowerCase() + "_" + i;
					meshes.push(part);
				}

			} else {
		
				let skel = this.readBones(model, hierarchy);
				let mesh = this.readGeometry(polygons, skel);
				mesh.geometry.animations = animations;
				mesh.userData.lod = key;
				mesh.name = name + "_" + key.toLowerCase();
				meshes.push(mesh);

			}
			
		}

		Viewport.API.setAsset(meshes);

	},

	readBones : function(model, hierarchy, debug) {

		if(!model.pose_ofs) {
			return;
		}
		
		const SCALE = 0.00125;
		const ROT = new THREE.Matrix4();
		ROT.makeRotationX(Math.PI);

		let bone_count = 0;
		hierarchy.forEach(rel => {
			if(rel.child_id <= bone_count) {
				return;
			}
			bone_count = rel.child_id;
		});

		bone_count++;
		let ofs = model.pose_ofs;
		let bone_ofs = this.MEM.view.getUint32(ofs + 0, true) & 0xffffff;

		let bones = [];
		let root_bone = new THREE.Bone()
		root_bone.name = "root";

		for(let i = 0; i < bone_count; i++) {

			let x = this.MEM.view.getInt16(bone_ofs + 0, true) * SCALE;
			let y = this.MEM.view.getInt16(bone_ofs + 2, true) * SCALE;
			let z = this.MEM.view.getInt16(bone_ofs + 4, true) * SCALE;

			let vec3 = new THREE.Vector3(x, y, z);
			vec3.applyMatrix4(ROT);
			
			if(i === 0 && debug) {
				console.log("--- Root Bone ---");
				console.log(vec3);
			}

			bone_ofs += 8;

			let bone = new THREE.Bone();
			bone.position.x = vec3.x;
			bone.position.y = vec3.y;
			bone.position.z = vec3.z;

			let str = i.toString();
			while(str.length < 3) {
				str = "0" + str;
			}
			bone.name = "bone_" + str;
			bones[i] = bone;

			if(i === 0) {
				root_bone.add(bone)
				continue;
			}

			let parent = hierarchy[i].parent_id;
			bones[parent].add(bone);
		}
		
		bones.unshift(root_bone);
		bones.forEach(bone=> {
			bone.updateMatrix();
			bone.updateMatrixWorld();
		});

		return bones;

	},

	readPolygons : function(ofs, hierarchy) {

		ofs += 0x14;

		let triOfs = this.MEM.view.getUint32(ofs + 0x04, true) & 0xffffff;
		let quadOfs = this.MEM.view.getUint32(ofs + 0x08, true) & 0xffffff;

		triOfs = triOfs || 0xffffff;
		quadOfs = quadOfs || 0xffffff;

		const end = triOfs < quadOfs ? triOfs : quadOfs;
		let polyCount = (end - ofs) / 0x14;

		if(polyCount < 0) {
			polyCount = hierarchy.length;
		} 
		
		let polygons = [];
		for(let i = 0; i < polyCount; i++) {

			let poly = {
				tri_count : this.MEM.view.getUint8(ofs + 0x00),
				quad_count : this.MEM.view.getUint8(ofs + 0x01),
				vert_count : this.MEM.view.getUint8(ofs + 0x02),
				poly_id : this.MEM.view.getUint8(ofs + 0x03),
				tri_ofs : this.MEM.view.getUint32(ofs + 0x04, true) & 0xffffff,
				quad_ofs : this.MEM.view.getUint32(ofs + 0x08, true) & 0xffffff,
				texture : this.MEM.view.getUint32(ofs + 0x0c, true),
				vert_ofs : this.MEM.view.getUint32(ofs + 0x10, true) & 0xffffff,
				weight : hierarchy[i] ? hierarchy[i].child_id + 1 : -1
			}

			if(poly.tri_ofs === 0) {
				poly.tri_count = 0;
			}

			if(poly.quad_ofs === 0) {
				poly.quad_count = 0;
			}

			if(poly.vert_ofs === 0) {
				poly.vert_count = 0;
			}

			ofs += 0x14;
			polygons.push(poly);
		}

		return polygons;

	},

	readGeometry : function(polygons, bones, forceMesh) {

		let ofs, face, geometry;
		geometry = new THREE.Geometry();
		
		let mats = [];
		let tex = [];
		for(let i = 0; i < polygons.length; i++) {
			if(tex.indexOf(polygons[i].texture) === -1) {
				tex.push(polygons[i].texture);
			}
			polygons[i].tex_id = tex.indexOf(polygons[i].texture);
		}

        for(let i = 0; i < tex.length; i++) {
            let canvas = Framebuffer.API.renderTexture(tex[i]);
            let texture = new THREE.Texture(canvas);
            texture.flipY = false;
            texture.needsUpdate = true;
            mats[i] = new THREE.MeshBasicMaterial({
                map : texture,
                skinning : true
            });
        }
		
		const SCALE = 0.00125;
		const ROT = new THREE.Matrix4();
		ROT.makeRotationX(Math.PI);
		
		polygons.forEach(poly => {

			ofs = poly.tri_ofs;
			for(let i = 0; i < poly.tri_count; i++) {

				let auv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x00) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x01) * 0.00390625 + 0.001953125)
				);

				let buv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x02) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x03) * 0.00390625 + 0.001953125)
				);

				let cuv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x04) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x05) * 0.00390625 + 0.001953125)
				);

				let a = this.MEM.view.getUint8(ofs + 0x08) + geometry.vertices.length;
				let b = this.MEM.view.getUint8(ofs + 0x09) + geometry.vertices.length;
				let c = this.MEM.view.getUint8(ofs + 0x0a) + geometry.vertices.length;

				face = new THREE.Face3(b, a, c);
				face.materialIndex = poly.tex_id;
				geometry.faces.push(face);
				geometry.faceVertexUvs[0].push([buv, auv, cuv]);
			
				ofs += 0x0c;
			}

			ofs = poly.quad_ofs;
			for(let i = 0; i < poly.quad_count; i++) {

				let auv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x00) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x01) * 0.00390625 + 0.001953125)
				);

				let buv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x02) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x03) * 0.00390625 + 0.001953125)
				);

				let cuv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x04) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x05) * 0.00390625 + 0.001953125)
				);

				let duv = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0x06) * 0.00390625 + 0.001953125,
					(this.MEM.view.getUint8(ofs + 0x07) * 0.00390625 + 0.001953125)
				);

				let a = this.MEM.view.getUint8(ofs + 0x08) + geometry.vertices.length;
				let b = this.MEM.view.getUint8(ofs + 0x09) + geometry.vertices.length;
				let c = this.MEM.view.getUint8(ofs + 0x0a) + geometry.vertices.length;
				let d = this.MEM.view.getUint8(ofs + 0x0b) + geometry.vertices.length;

				face = new THREE.Face3(b, a, c);
				face.materialIndex = poly.tex_id;
				geometry.faces.push(face);
				geometry.faceVertexUvs[0].push([buv, auv, cuv]);

				face = new THREE.Face3(d, b, c);
				face.materialIndex = poly.tex_id;
				geometry.faces.push(face);
				geometry.faceVertexUvs[0].push([duv, buv, cuv]);
		
				ofs += 0x0c;
			}

			ofs = poly.vert_ofs;
			for(let i = 0; i < poly.vert_count; i++) {

				let x = this.MEM.view.getInt16(ofs + 0, true) * SCALE;
				let y = this.MEM.view.getInt16(ofs + 2, true) * SCALE;
				let z = this.MEM.view.getInt16(ofs + 4, true) * SCALE;

				let vertex = new THREE.Vector3(x, y, z);
				vertex.applyMatrix4(ROT);
				geometry.vertices.push(vertex);
				ofs += 8;

				if(!bones || poly.weight === -1) {
					continue;
				}
				
				let boneId = poly.weight;
				geometry.skinIndices.push(new THREE.Vector4(boneId, 0, 0, 0));
				geometry.skinWeights.push(new THREE.Vector4(1, 0, 0, 0));
				vertex.applyMatrix4(bones[boneId].matrixWorld);
			}

		});

		geometry.computeFaceNormals();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);

	   	let mesh;

	   	if(!bones || forceMesh) {
		   	mesh = new THREE.Mesh(buffer, mats);
	   	} else {
		   	mesh = new THREE.SkinnedMesh(buffer, mats);
		   	let armSkeleton = new THREE.Skeleton(bones);
		   	let rootBone = armSkeleton.bones[0];
		   	mesh.add(rootBone);
	   		mesh.bind(armSkeleton);
		}

		mesh.userData.faces = geometry.faces.length;
		mesh.userData.vertices = geometry.vertices.length;
		return mesh;

	},

	readAnimations : function(model, bones, debug) {

		if(!model.pose_ofs) {
			return;
		}

		let animations = [];
		const SCALE = 0.00125;
		const FPS = 30;
		const ROT = new THREE.Matrix4();
		ROT.makeRotationX(Math.PI);

		// First we read the controller

		let time_ofs = model.time_ofs;
		let first = this.MEM.view.getUint32(time_ofs, true) & 0xffffff;
		let count = (first - time_ofs) / 4;
		// count--;
		// time_ofs += 4;
		let pose_ofs = model.pose_ofs + 4;
		let anims = [];
		let animCount = 0;

		if(debug) {
			console.log("doing anim debug");
		}

		for(let i = 0; i < count; i++) {

			let anim = {
				time : this.MEM.view.getUint32(time_ofs, true) & 0xffffff,
				pose : this.MEM.view.getUint32(pose_ofs, true) & 0xffffff,
				timing : [],
				frames : 0,
				max : 0
			};

			time_ofs += 4;
			pose_ofs += 4;

			if(anim.time === 0) {
				continue;
			}
			animCount++;

			// Get timings

			let ptr = anim.time;
			let num, pose, frames, frame, end;
			num = 0;
			end = 0;
			frames = 0;

			let firstPose = this.MEM.view.getUint32(anim.pose, true) & 0xffffff;
			while(end !== -1 && end !== -128) {
				pose = this.MEM.view.getUint8(ptr);
				frame = this.MEM.view.getUint8(ptr + 1);
				end = this.MEM.view.getInt8(ptr + 3);
				let term = this.MEM.view.getUint8(ptr + 3);

				console.log('Term: 0x%s', term.toString(16));

				ptr += 8;

				anim.timing.push({
					pose : pose,
					frame : frame,
					time : frames / FPS
				});

				if(term & 0x80) {
					break;
				}

				frames += 1;

			}

			// Read all of the pose tracks

			let ofs = anim.pose;
			let ptrList = [];

			const max = (firstPose - anim.pose) / 4;
			for(let k = 0; k < max; k++) {
				ptrList[k] = this.MEM.view.getUint32(ofs, true) & 0xffffff;
				ofs += 4;
			}

			let ptrs = [];
			for(let k = 0; k < anim.timing.length; k++) {
				anim.timing[k].ofs = ptrList[anim.timing[k].pose];
			}

			let str = animations.length.toString();
			while(str.length < 3) {
				str = "0" + str;
			}

			const ANIM = {
				name : "anim_" + str,
				fps : FPS,
				length : frames / FPS,
				hierarchy : []
			}

			console.log(ANIM.name);

			for(let k = 0; k < bones.length; k++) {
				ANIM.hierarchy.push({
					parent : k - 1,
					keys : []
				});
			}

			anim.timing.forEach(nop => {
				
				ofs = nop.ofs;
				let view = this.MEM.view;

				let pos = {
					x : view.getUint16(ofs + 0, true) & 0xFFF,
					y : view.getUint16(ofs + 1, true) >> 4,
					z : view.getUint16(ofs + 3, true) & 0xFFF,
				};
				
				let og_y = pos.y;
				
				if (pos.x & 0x800) {
					pos.x = (0x800 - (pos.x & 0x7ff));
				}

				/*
				if (pos.y & 0x800) {
					pos.y = (0x800 - (pos.y & 0x7ff));
				}
				*/
				
				if (pos.z & 0x800) {
					pos.z = (0x800 - (pos.z & 0x7ff));
				}
				
				pos.x *= SCALE;
				// pos.y *= SCALE;
				pos.z *= SCALE;
				
				if(pos.y !== 0) {
					pos.y = (0xfff - pos.y) * 0.01;
				}

				let vec3 = new THREE.Vector3(pos.x, pos.y, pos.z);
				vec3.applyMatrix4(ROT);
				ofs += 4;

				ANIM.hierarchy[0].keys.push({
					time : nop.time,
					rot : [0,0,0,1],
					scl : [1, 1, 1],
					pos : [0,0,0]
				});

				for(let k = 1; k < bones.length; k++) {
					
					let r;
					if ((k % 2) === 1) {
						r = {
							x : (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360,
							y : (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360,
							z : (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360,
						};
						ofs += 5;
					} else {
						r = {
							x : (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360,
							y : (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360,
							z : (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360,
						};
						ofs += 4;
					}

					let e = new THREE.Euler(
						r.x * Math.PI / 180,
						-r.y * Math.PI / 180,
						-r.z * Math.PI / 180
					);

					let q = new THREE.Quaternion();
					q.setFromEuler(e);

					let key = {
						time : nop.time,
						rot : q.toArray(),
						scl : [1, 1, 1],
						pos : [
							bones[k].position.x,
							bones[k].position.y,
							bones[k].position.z
						]
					};
					
					if(k === 1) {
						// key.pos[1] = pos.y;
						key.pos[1] = pos.y;
						console.log(nop.time.toFixed(2), '0x' + og_y.toString(16), pos.y);
					}

					ANIM.hierarchy[k].keys.push(key);
				}

			});
			
			if(ANIM.hierarchy[0].keys.length === 1) {
				continue;
			}

			let clip = THREE.AnimationClip.parseAnimation(ANIM, bones);
			if(!clip) {
				continue;
			}
			clip.optimize();
			animations.push(clip);
			
		}
		
		return animations;

	}

}
