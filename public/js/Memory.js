const Memory = (function() {

	this.MEM = {
		db : new Dexie("mml1")
	}

	this.DOM = {
		input :  document.getElementById('Memory.input'),
		folder : document.getElementById('Memory.folder'),
		tray : document.getElementById('Memory.tray')
	}

	this.EVT = {
		handleInputChange : evt_handleInputChange.bind(this),
		handleFolderClick : evt_handleFolderClick.bind(this),
		handleTrayClick : evt_handleTrayClick.bind(this)
	}

	this.API = {
		prepareState : api_prepareState.bind(this)
	}

	init.apply(this);
	return this;

	async function init() {
		
		this.MEM.db.version(1).stores({
			cache : '&name'
		});

		this.DOM.input.addEventListener('change', this.EVT.handleInputChange);
		this.DOM.folder.addEventListener('click', this.EVT.handleFolderClick);
		this.DOM.tray.addEventListener('click', this.EVT.handleTrayClick);

		let state = await this.MEM.db.cache.get({name:'state'});
		if(!state) {
			return;
		}

		this.MEM.state = state;
		this.API.prepareState();

	}

	function evt_handleInputChange(evt) {

		let file = evt.target.files[0];

		console.log(file);

		let reader = new FileReader();

		reader.onload = (evt) => {

			let array = evt.target.result;
			const compressed = new Uint8Array(array);
			const gunzip = new Zlib.Gunzip(compressed);
			const sstate = gunzip.decompress();
			
			const query = {
				name : 'state',
				psxm : new Uint8Array(0x200000),
				vram : new Uint8Array(0x100000)
			};

			const cpu_ofs = 0x9025;
			const gpu_ofs = 0x29B749;

			for(let i = 0; i < query.psxm.length; i++) {
				query.psxm[i] = sstate[cpu_ofs + i];
			}

			for(let i = 0; i < query.vram.length; i++) {
				query.vram[i] = sstate[gpu_ofs + i];
			}
			
			this.MEM.db.cache.put(query);
			this.MEM.state = query;
			this.API.prepareState();

		}

		reader.readAsArrayBuffer(file);

	}

	function evt_handleFolderClick(evt) {
		
		console.log("folder click!!!");
		let type = evt.target.getAttribute("data-type");

		if(!type) {
			return;
		}
		
		if(this.MEM.activeFolder) {
			this.MEM.activeFolder.classList.remove('active');
		}

		this.MEM.activeFolder = evt.target;
		this.MEM.activeFolder.classList.add('active');
		
		while(this.DOM.tray.childNodes.length) {
			this.DOM.tray.removeChild(this.DOM.tray.childNodes[0]);
		}

		this.DOM.tray.appendChild(this.DOM[type]);

	}

	function evt_handleTrayClick(evt) {

		console.log("tray click");

		let elem = evt.target;
		if(elem.tagName !== 'LI') {
			return;
		}
		
		if(this.MEM.activeLi && this.MEM.activeLi === elem) {
			return;
		}
	
		console.log("here!!");

		if(this.MEM.activeLi) {
			this.MEM.activeLi.classList.remove("active");
		}

		this.MEM.activeLi = elem;
		this.MEM.activeLi.classList.add("active");

		let asset;
		let type = elem.getAttribute("data-type");
		let name = elem.getAttribute("data-name");
		Viewport.MEM.asset_name = elem.textContent;

		switch(type) {
		case "ebd":
			asset = EbdLoader.fetchAsset(name);
			break;
		case "ply":
			asset = PbdLoader.fetchAsset(name);
			break;
		case "stg":
			asset = StgLoader.fetchAsset(name);
			break;
		case "mdt":
			asset = MdtLoader.fetchAsset(name);
			break;
		}

	}

	function api_prepareState() {
		
		console.log("preparing state");
		Framebuffer.API.prepare(this.MEM.state.vram);
		this.DOM.ebd = EbdLoader.setState(this.MEM.state.psxm);
		this.DOM.ply = PbdLoader.setState(this.MEM.state.psxm);
		this.DOM.stg = StgLoader.setState(this.MEM.state.psxm);
		this.DOM.mdt = MdtLoader.setState(this.MEM.state.psxm);

		// saveAs(new Blob([this.MEM.state.psxm]), "motb.mem");
	
		let type = "ebd";
		if(!this.MEM.activeFolder) {
			this.MEM.activeFolder = this.DOM.folder.children[0];
			this.MEM.activeFolder.classList.add('active');
		} else {
			type = this.MEM.activeFolder.getAttribute("data-type");
		}
		
		while(this.DOM.tray.childNodes.length) {
			this.DOM.tray.removeChild(this.DOM.tray.childNodes[0]);
		}

		this.DOM.tray.appendChild(this.DOM[type]);

	}

}).apply({});
