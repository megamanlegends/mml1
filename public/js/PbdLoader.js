"use strict";

const PbdLoader = {

	setState: function(mem) {

		this.MEM = {};
		this.MEM.mem = mem;
		this.MEM.view = new DataView(mem.buffer);
		this.MEM.assets = {};

		const ul = document.createElement('ul');
		const li = document.createElement('li');
		li.setAttribute("data-type", "ply");
		li.setAttribute("data-name", "megaman");
		li.textContent = "Megaman";
		ul.appendChild(li);

		return ul;

	},

	fetchAsset: function(name) {
		
		let mesh = this.readFull();
		let body = this.readBase();
		let list = [ mesh, body ];
		this.readParts(list);
		Viewport.API.setAsset(list);

	},

	readParts : function(list) {

		console.log("calling %s", name);

		// Prepare Materials

		const materials = [1006632997, 1044906031];
		for (let i = 0; i < materials.length; i++) {
			let canvas = Framebuffer.API.renderTexture(materials[i]);
			let texture = new THREE.Texture(canvas);
			texture.flipY = false;
			texture.needsUpdate = true;
			materials[i] = new THREE.MeshBasicMaterial({
				map: texture,
				skinning: true
			});
		}

		// Prepare Ploygons

		const polygons = {
			"left" : this.readLeftArm([]),
			"right" : this.readRightArms([]),
			"feet" : this.readFeet([]),
			"buster" : this.readBusterArm([]),
			"special" : this.readSpecialArm([])
		};

		for(let key in polygons) {
			
			polygons[key].forEach(poly => {

				let geometry = new THREE.Geometry();
				this.MEM.geometry = geometry;

				let matIndex = poly.matId || 0;
				this.MEM.vertexOfs = this.MEM.geometry.vertices.length;
				this.readTriList(poly.tri_count, poly.triOfs, matIndex);
				this.readQuadList(poly.quad_count, poly.quadOfs, matIndex);
				this.readVertices(poly.vert_count, poly.vertOfs);
	
				if(geometry.faces.length < 4) {
					return;
				}

				geometry.computeFaceNormals();
   				let buffer = new THREE.BufferGeometry();
				buffer.fromGeometry(geometry);

				let mesh = new THREE.Mesh(buffer, materials);
				mesh.name = key + "_" + poly.bone_id;
				mesh.userData.lod = "High";
				mesh.userData.faces = geometry.faces.length;
				mesh.userData.vertices = geometry.vertices.length;
				list.push(mesh);

			});
		}

		let geometry = new THREE.Geometry();
		this.MEM.geometry = geometry;
		
		let head = this.readHead([]);
		head.forEach(poly => {

			let matIndex = poly.matId || 0;
			this.MEM.vertexOfs = this.MEM.geometry.vertices.length;
			this.readTriList(poly.tri_count, poly.triOfs, matIndex);
			this.readQuadList(poly.quad_count, poly.quadOfs, matIndex);
			this.readVertices(poly.vert_count, poly.vertOfs);

		});
	
		geometry.computeFaceNormals();
   		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);

		let mesh = new THREE.Mesh(buffer, materials);
		mesh.name = "Head";
		mesh.userData.lod = "High";
		mesh.userData.faces = geometry.faces.length;
		mesh.userData.vertices = geometry.vertices.length;
		list.push(mesh);

	},

	readFull : function() {

		console.log("calling %s", name);

		// Prepare Materials

		const materials = [1006632997, 1044906031];
		for (let i = 0; i < materials.length; i++) {
			let canvas = Framebuffer.API.renderTexture(materials[i]);
			let texture = new THREE.Texture(canvas);
			texture.flipY = false;
			texture.needsUpdate = true;
			materials[i] = new THREE.MeshBasicMaterial({
				map: texture,
				skinning: true
			});
		}

		// Prepare Ploygons

		const polygons = [];

		this.readBody(polygons);
		this.readHead(polygons);
		this.readLeftArm(polygons);
		this.readRightArms(polygons);
		this.readFeet(polygons);

		let bones = this.readBones();
		let geometry = new THREE.Geometry();
		this.MEM.geometry = geometry;
		let anims = this.readAnimations(bones);

		polygons.forEach(poly => {

			let matIndex = poly.matId || 0;
			this.MEM.vertexOfs = this.MEM.geometry.vertices.length;
			this.readTriList(poly.tri_count, poly.triOfs, matIndex);
			this.readQuadList(poly.quad_count, poly.quadOfs, matIndex);
			this.readVertices(poly.vert_count, poly.vertOfs, poly.boneId, bones);

		});
	
		geometry.computeFaceNormals();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);

		// Create mesh

		let mesh = new THREE.SkinnedMesh(buffer, materials);
		mesh.name = "megaman";
		mesh.userData.lod = "High";
		mesh.userData.faces = geometry.faces.length;
		mesh.userData.vertices = geometry.vertices.length;
		mesh.geometry.animations = anims;

		let armSkeleton = new THREE.Skeleton(bones);
		let rootBone = armSkeleton.bones[0];
		mesh.add(rootBone);
		mesh.bind(armSkeleton);
		return mesh;


	},

	readBase : function() {

		console.log("calling %s", name);

		// Prepare Materials

		const materials = [1006632997, 1044906031];
		for (let i = 0; i < materials.length; i++) {
			let canvas = Framebuffer.API.renderTexture(materials[i]);
			let texture = new THREE.Texture(canvas);
			texture.flipY = false;
			texture.needsUpdate = true;
			materials[i] = new THREE.MeshBasicMaterial({
				map: texture,
				skinning: true
			});
		}

		// Prepare Ploygons

		const polygons = [];

		this.readBody(polygons);

		let bones = this.readBones();
		let geometry = new THREE.Geometry();
		this.MEM.geometry = geometry;
		let anims = this.readAnimations(bones);

		polygons.forEach(poly => {

			let matIndex = poly.matId || 0;
			this.MEM.vertexOfs = this.MEM.geometry.vertices.length;
			this.readTriList(poly.tri_count, poly.triOfs, matIndex);
			this.readQuadList(poly.quad_count, poly.quadOfs, matIndex);
			this.readVertices(poly.vert_count, poly.vertOfs, poly.boneId, bones);

		});
	
		geometry.computeFaceNormals();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(geometry);

		// Create mesh

		let mesh = new THREE.SkinnedMesh(buffer, materials);
		mesh.name = "megaman_base";
		mesh.userData.lod = "High";
		mesh.userData.faces = geometry.faces.length;
		mesh.userData.vertices = geometry.vertices.length;
		mesh.geometry.animations = anims;

		let armSkeleton = new THREE.Skeleton(bones);
		let rootBone = armSkeleton.bones[0];
		mesh.add(rootBone);
		mesh.bind(armSkeleton);
		return mesh;

	},

	readBody: function(polygons) {

		const BODY_OFS = 0x00f5600;

		let ofs = BODY_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if (first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}

		let count = (first - ofs) / 0x10;
		let weights = [0, 8, 9, 10, 12, 13];

		for (let i = 0; i < count; i++) {
			polygons.push({
				name: "body",
				tri_count: this.MEM.view.getUint8(ofs + 0),
				quad_count: this.MEM.view.getUint8(ofs + 1),
				vert_count: this.MEM.view.getUint8(ofs + 2),
				id: this.MEM.view.getUint8(ofs + 3),
				triOfs: this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs: this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs: this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId: weights[i],
				matId: 0
			});
			ofs += 16;
		}


	},

	readHead: function(polygons) {

		const HEAD_OFS = 0x00f7800;

		let ofs = HEAD_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if (first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}
		let count = (first - ofs) / 0x10;
		let matId = [0, 1, 1, 1];

		let face = [];
		for (let i = 0; i < count; i++) {
			face.push({
				name: "head" + i,
				tri_count: this.MEM.view.getUint8(ofs + 0),
				quad_count: this.MEM.view.getUint8(ofs + 1),
				vert_count: this.MEM.view.getUint8(ofs + 2),
				id: this.MEM.view.getUint8(ofs + 3),
				triOfs: this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs: this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs: this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId: 1,
				matId: matId[i]
			});
			ofs += 16;
		}

		if (face[0].tri_count === 68) {
			face[0].matId = 1;
		}

		face.forEach(p => {
			polygons.push(p);
		});

		return polygons;
	},

	readLeftArm: function(polygons) {

		const LEFTARM_OFS = 0x00fa000;

		let ofs = LEFTARM_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if (first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}

		let weights = [2, 3, 4];
		let count = (first - ofs) / 0x10;

		for (let i = 0; i < count; i++) {
			polygons.push({
				name: "left arm" + i,
				tri_count: this.MEM.view.getUint8(ofs + 0),
				quad_count: this.MEM.view.getUint8(ofs + 1),
				vert_count: this.MEM.view.getUint8(ofs + 2),
				id: this.MEM.view.getUint8(ofs + 3),
				triOfs: this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs: this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs: this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId: weights[i],
				matId: 0
			});
			ofs += 16;
		}
		
		return polygons;

	},

	readRightArms: function(polygons) {

		const SPECIALWPN_OFS = 0x00fb500;
		const RIGHTARM_OFS = 0x00fca00;

		let ofs = RIGHTARM_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;
		if (first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}
		let count = (first - ofs) / 0x10;
		let weights = [5, 6, 7];

		for (let i = 0; i < count; i++) {
			polygons.push({
				name: "body",
				tri_count: this.MEM.view.getUint8(ofs + 0),
				quad_count: this.MEM.view.getUint8(ofs + 1),
				vert_count: this.MEM.view.getUint8(ofs + 2),
				id: this.MEM.view.getUint8(ofs + 3),
				triOfs: this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs: this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs: this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId: weights[i],
				matId: 0
			});
			ofs += 16;
		}

		return polygons;

	},

	readFeet: function(polygons) {

		const FEET_OFS = 0x00ff400;

		let ofs = FEET_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if (first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}

		let count = (first - ofs) / 0x10;
		let weights = [11, 14];

		for (let i = 0; i < count; i++) {
			polygons.push({
				name: "body",
				tri_count: this.MEM.view.getUint8(ofs + 0),
				quad_count: this.MEM.view.getUint8(ofs + 1),
				vert_count: this.MEM.view.getUint8(ofs + 2),
				id: this.MEM.view.getUint8(ofs + 3),
				triOfs: this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs: this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs: this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId: weights[i],
				matId: 0
			});
			ofs += 16;
		}

		return polygons;
	},

	readTriList: function(nbFace, ofs, matId) {

		if (ofs < 0) {
			return;
		}

		let materialIndex = matId || 0;
		let uv = new Array(3);

		for (let i = 0; i < nbFace; i++) {

			ofs += 40;

			for (let k = 0; k < uv.length; k++) {
				ofs += 4;
				uv[k] = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0) * 0.00390625 + 0.001953125,
					this.MEM.view.getUint8(ofs + 1) * 0.00390625 + 0.001953125
				);
				ofs += 4;
			}

			let a = this.MEM.view.getUint8(ofs + 0) + this.MEM.vertexOfs;
			let b = this.MEM.view.getUint8(ofs + 1) + this.MEM.vertexOfs;
			let c = this.MEM.view.getUint8(ofs + 2) + this.MEM.vertexOfs;
			ofs += 4;

			let face = new THREE.Face3(b, a, c);
			face.materialIndex = matId;
			this.MEM.geometry.faces.push(face);

			this.MEM.geometry.faceVertexUvs[0].push([uv[1], uv[0], uv[2]]);

		}

	},

	readQuadList: function(nbFace, ofs, matId) {

		if (ofs < 0) {
			return;
		}

		let materialIndex = matId || 0;
		let uv = new Array(4);

		for (let i = 0; i < nbFace; i++) {

			ofs += 48;

			for (let k = 0; k < uv.length; k++) {
				ofs += 4;
				uv[k] = new THREE.Vector2(
					this.MEM.view.getUint8(ofs + 0) * 0.00390625 + 0.001953125,
					this.MEM.view.getUint8(ofs + 1) * 0.00390625 + 0.001953125
				);
				ofs += 4;
			}

			let a = this.MEM.view.getUint8(ofs + 0) + this.MEM.vertexOfs;
			let b = this.MEM.view.getUint8(ofs + 1) + this.MEM.vertexOfs;
			let c = this.MEM.view.getUint8(ofs + 2) + this.MEM.vertexOfs;
			let d = this.MEM.view.getUint8(ofs + 3) + this.MEM.vertexOfs;
			ofs += 4;

			let face = new THREE.Face3(b, a, c);
			face.materialIndex = matId;
			this.MEM.geometry.faces.push(face);

			face = new THREE.Face3(d, b, c);
			face.materialIndex = matId;
			this.MEM.geometry.faces.push(face);

			this.MEM.geometry.faceVertexUvs[0].push([uv[1], uv[0], uv[2]]);
			this.MEM.geometry.faceVertexUvs[0].push([uv[3], uv[1], uv[2]]);

		}
	},

	readVertices: function(count, ofs, boneId, bones) {

		const SCALE = 0.00125;
		const ROT = new THREE.Matrix4().makeRotationX(Math.PI);

		for (let i = 0; i < count; i++) {

			let x = this.MEM.view.getInt16(ofs + 0, true) * SCALE;
			let y = this.MEM.view.getInt16(ofs + 2, true) * SCALE;
			let z = this.MEM.view.getInt16(ofs + 4, true) * SCALE;

			let vertex = new THREE.Vector3(x, y, z);
			vertex.applyMatrix4(ROT);

			if(bones) {

				if (boneId !== -1) {
					vertex.applyMatrix4(bones[boneId].matrixWorld);
				}

				this.MEM.geometry.skinIndices.push(new THREE.Vector4(boneId, 0, 0, 0));
				this.MEM.geometry.skinWeights.push(new THREE.Vector4(1, 0, 0, 0));
			
			}
	
			this.MEM.geometry.vertices.push(vertex);
			ofs += 8;

		}
	},

	readBones: function() {

		const boneSrc = [{
				"pos": {
					"x": 0,
					"y": 0.90625,
					"z": -1.1098361617272889e-16
				},
				"name": "bone_000",
				"parent": -1
			},
			{
				"pos": {
					"x": 0,
					"y": 0.2575,
					"z": -3.153465507804434e-17
				},
				"name": "bone_001",
				"parent": 0
			},
			{
				"pos": {
					"x": -0.17500000000000002,
					"y": 0.1625,
					"z": -0.01250000000000002
				},
				"name": "bone_002",
				"parent": 0
			},
			{
				"pos": {
					"x": -0.025,
					"y": -0.2,
					"z": 2.4492935982947065e-17
				},
				"name": "bone_003",
				"parent": 2
			},
			{
				"pos": {
					"x": 0,
					"y": -0.1525,
					"z": 1.8675863686997135e-17
				},
				"name": "bone_004",
				"parent": 3
			},
			{
				"pos": {
					"x": 0.17500000000000002,
					"y": 0.1625,
					"z": -0.01250000000000002
				},
				"name": "bone_005",
				"parent": 0
			},
			{
				"pos": {
					"x": 0.025,
					"y": -0.2,
					"z": 2.4492935982947065e-17
				},
				"name": "bone_006",
				"parent": 5
			},
			{
				"pos": {
					"x": 0,
					"y": -0.1525,
					"z": 1.8675863686997135e-17
				},
				"name": "bone_007",
				"parent": 6
			},
			{
				"pos": {
					"x": 0,
					"y": 0,
					"z": 0
				},
				"name": "bone_008",
				"parent": 0
			},
			{
				"pos": {
					"x": -0.08125,
					"y": -0.09125,
					"z": 1.1174902042219598e-17
				},
				"name": "bone_009",
				"parent": 8
			},
			{
				"pos": {
					"x": 0,
					"y": -0.28125,
					"z": 3.444319122601931e-17
				},
				"name": "bone_010",
				"parent": 9
			},
			{
				"pos": {
					"x": 0,
					"y": -0.34875,
					"z": 4.2709557120263944e-17
				},
				"name": "bone_011",
				"parent": 10
			},
			{
				"pos": {
					"x": 0.08125,
					"y": -0.09125,
					"z": 1.1174902042219598e-17
				},
				"name": "bone_012",
				"parent": 8
			},
			{
				"pos": {
					"x": 0,
					"y": -0.28125,
					"z": 3.444319122601931e-17
				},
				"name": "bone_013",
				"parent": 12
			},
			{
				"pos": {
					"x": 0,
					"y": -0.34875,
					"z": 4.2709557120263944e-17
				},
				"name": "bone_014",
				"parent": 13
			}
		];

		let bones = [];
		boneSrc.forEach(src => {

			let bone = new THREE.Bone();

			bone.position.x = src.pos.x;
			bone.position.y = src.pos.y;
			bone.position.z = src.pos.z;
			bone.name = src.name;

			if (bones[src.parent]) {
				bones[src.parent].add(bone);
			}

			bones.push(bone);

		});

		bones.forEach(bone => {
			bone.updateMatrix();
			bone.updateMatrixWorld();
		});

		return bones;

	},

	readAnimations: function(bones) {

		let model;
		let anims = [];

		// Standard animations
		
		try {
			model = {
				poseOfs: 0xDD600,
				animOfs: 0xD9D00
			};
			this.readAnims(model, bones, anims, true);
		} catch (err) {
			console.error(err);
		}

		try {
			model = {
				poseOfs: 0xE9600,
				animOfs: 0xDBA00
			};
			this.readAnims(model, bones, anims);
		} catch (err) {

		}
		
		// Special Weapon animation (right kick)
	
		try {
			model = {
				poseOfs: 0xED600,
				animOfs: 0xDC400
			};
			this.readAnims(model, bones, anims);
		} catch (err) {

		}

		try {
			model = {
				poseOfs: 0xDC880,
				animOfs: 0xEF600
			};
			this.readAnims(model, bones, anims);
		} catch (err) {

		}
		
		try {
			model = {
				poseOfs: 0xEF600,
				animOfs: 0xDC880
			};
			this.readAnims(model, bones, anims);
		} catch (err) {

		}

		// Left kick 

		try {
			model = {
				poseOfs: 0xF1600,
				animOfs: 0xDCD00
			};
			this.readAnims(model, bones, anims);
		} catch (err) {

		}

		// Buster Animations

		try {
			model = {
				poseOfs: 0xF3600,
				animOfs: 0xDD180
			};
			this.readAnims(model, bones, anims);
		} catch (err) {

		}

		return anims;
	},

	readAnims: function(model, bones, anim_list, do_add) {

		const SCALE = 0.00125;
		const ROT = new THREE.Matrix4().makeRotationX(Math.PI);

		if (model.animOfs === 0) {
			console.log("Something is not right here");
			return;
		}

		// First we read the controller

		let animOfs = model.animOfs;
		let first = this.MEM.view.getUint32(animOfs, true) & 0xffffff;

		let count = (first - animOfs) / 4;
		let poseOfs = model.poseOfs;
		if(do_add) {
			poseOfs += 4;
		}
		let anims = [];
		let animCount = 0;

		for (let i = 0; i < count; i++) {
		
			console.log("Reading time at 0x%s", animOfs.toString(16));
			console.log("Reading pose at 0x%s", poseOfs.toString(16));

			let anim = {
				time: this.MEM.view.getUint32(animOfs, true) & 0xffffff,
				pose: this.MEM.view.getUint32(poseOfs, true) & 0xffffff,
				timing: [],
				frames: 0,
				max: 0
			};

			console.log(anim);

			animOfs += 4;
			poseOfs += 4;

			if (anim.time === 0) {
				continue;
			}

			animCount++;

			// Get timings

			const FPS = 30;

			let ptr = anim.time;
			let num, pose, frames, frame, end, max;
			num = 0;
			end = 0;
			max = 0;
			frames = 0;

			let firstPose = this.MEM.view.getUint32(anim.pose, true) & 0xffffff;
			max = (firstPose - anim.pose) / 4;

			while (end !== -1 && end !== -128) {
				pose = this.MEM.view.getUint8(ptr);
				frame = this.MEM.view.getUint8(ptr + 1);
				end = this.MEM.view.getInt8(ptr + 3);
				ptr += 8;

				if (pose >= max) {
					continue;
				}

				anim.timing.push({
					pose: pose,
					frame: frame,
					time: frames / 30
				});
				frames += frame;

				if (end & 0x80) {
					break;
				}

			}
			max++;

			// Read all of the pose tracks

			let ofs = anim.pose;
			let ptrList = [];

			for (let k = 0; k < max; k++) {
				ptrList[k] = this.MEM.view.getUint32(ofs, true) & 0xffffff;
				//ptrList[k] = ptrList[k].toString(16);
				ofs += 4;
			}

			let ptrs = [];
			for (let k = 0; k < anim.timing.length; k++) {
				anim.timing[k].ofs = ptrList[anim.timing[k].pose];
			}

			// Start creating animation

			let str = anim_list.length.toString();
			while (str.length < 3) {
				str = "0" + str;
			}

			const ANIM = {
				name: "anim_" + str,
				fps: FPS,
				length: frames / FPS,
				hierarchy: []
			}
			console.log(ANIM.name);

			for (let k = 0; k < bones.length; k++) {
				ANIM.hierarchy.push({
					parent: k - 1,
					keys: []
				});
			}

			anim.timing.forEach(nop => {

				ofs = nop.ofs;
				let view = this.MEM.view;

				for (let k = 0; k < bones.length; k++) {

					let key = {
						time: nop.time,
						rot: {
							x: 0,
							y: 0,
							z: 0,
						},
						scl: [1, 1, 1],
						pos: [
							bones[k].position.x,
							bones[k].position.y,
							bones[k].position.z
						]
					};

					switch (k) {
						case 0:

							let pos = {
								x: view.getInt16(ofs + 0, true) & 0xFFF,
								y: view.getInt16(ofs + 1, true) >> 4,
								z: view.getInt16(ofs + 3, true) & 0xFFF,
							};
							ofs += 4;

							pos.x *= SCALE;
							pos.y *= SCALE;
							pos.z *= SCALE;

							let vec3 = new THREE.Vector3(pos.x, pos.y, pos.z);
							vec3.applyMatrix4(ROT);

							//key.pos[0] = vec3.x;
							key.pos[1] = vec3.y;
							//key.pos[2] = vec3.z;

							key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
							key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
							ofs += 5;

							break;
						case 1:

							key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
							key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;

							ofs += 4;
							break;
						case 2:

							key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
							key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
							ofs += 5;

							break;
						case 3:

							key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;

							break;
						case 4:

							key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
							ofs += 4;

							break;
						case 5:

							key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
							key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
							ofs += 5;

							break;
						case 6:

							key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;

							break;
						case 7:

							key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;
							ofs += 4;

							break;
						case 8:

							key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
							key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
							ofs += 5;

							break;
						case 9:

							key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
							key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;

							ofs += 4;
							break;
						case 10:

							key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
							ofs += 2;
							break;
						case 11:

							key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
							key.rot.y = (view.getUint16(ofs + 1, true) >> 4) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) & 0xFFF) / 0xFFF * 360;

							ofs += 4;

							break;
						case 12:

							key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
							key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;
							ofs += 5;

							break;
						case 13:

							if (nop.ofs === 0xdd7f8) {
								console.log("Offset: 0x%s", ofs.toString(16));
							}
							key.rot.x = (view.getUint16(ofs + 0, true) & 0xFFF) / 0xFFF * 360;
							ofs += 1;

							break;
						case 14:

							key.rot.x = (view.getUint16(ofs + 0, true) >> 4) / 0xFFF * 360;
							key.rot.y = (view.getUint16(ofs + 2, true) & 0xFFF) / 0xFFF * 360;
							key.rot.z = (view.getUint16(ofs + 3, true) >> 4) / 0xFFF * 360;

							break;
					}

					let e = new THREE.Euler(
						key.rot.x * Math.PI / 180,
						-key.rot.y * Math.PI / 180,
						-key.rot.z * Math.PI / 180
					);

					let q = new THREE.Quaternion();
					q.setFromEuler(e);
					key.rot = q;

					ANIM.hierarchy[k].keys.push(key);
				}

			});

			let clip = THREE.AnimationClip.parseAnimation(ANIM, bones);

			if (!clip) {
				continue;
			}

			clip.optimize();
			anim_list.push(clip);

		}

		return anim_list;

	},

	readBusterArm :function (polygons) {

		const BUSTER_OFS = 0x00fdf00;

		let ofs = BUSTER_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if(first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}

		let weights = [ 5, 6, 7 ];
		let count = (first - ofs) / 0x10;

		// let left = [];
		// this.API.readLeftArm(left);

		let buster = [];
		for(let i = 0; i < count; i++) {
			polygons.push({
				name : "buster" + i,
				tri_count : this.MEM.view.getUint8(ofs + 0),
				quad_count : this.MEM.view.getUint8(ofs + 1),
				vert_count : this.MEM.view.getUint8(ofs + 2),
				id : this.MEM.view.getUint8(ofs + 3),
				triOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId : weights[i],
				matId : 0
			});
			ofs += 16;
		}

		return polygons;
	},

	readSpecialArm : function(polygons) {

		const SPECIALWPN_OFS = 0x00fb500;

		let ofs = SPECIALWPN_OFS;
		let first = this.MEM.view.getUint32(ofs + 4, true) & 0xffffff;

		if(first === 0) {
			first = this.MEM.view.getUint32(ofs + 8, true) & 0xffffff;
		}
		let count = (first - ofs) / 0x10;
		let weights = [ 2, 3, 4];

		for(let i = 0; i < count; i++) {
			polygons.push({
				name : "special_arm" + i,
				tri_count : this.MEM.view.getUint8(ofs + 0),
				quad_count : this.MEM.view.getUint8(ofs + 1),
				vert_count : this.MEM.view.getUint8(ofs + 2),
				id : this.MEM.view.getUint8(ofs + 3),
				triOfs : this.MEM.view.getUint32(ofs + 4, true) & 0xffffff,
				quadOfs : this.MEM.view.getUint32(ofs + 8, true) & 0xffffff,
				vertOfs : this.MEM.view.getUint32(ofs + 12, true) & 0xffffff,
				boneId : weights[i],
				matId : 0
			});
			ofs += 16;
		}

		return polygons;
	}

}
