/*
 * @author - Kion
 */

"use strict";

const Framebuffer = (function() {

	this.MEM = {};

	this.DOM = {
		canvas : document.getElementById('Framebuffer.canvas')
	};

	this.EVT = {};

	this.API = {
		prepare : api_prepare.bind(this),
		renderAll : api_renderAll.bind(this),
		renderTexture : api_renderTexture.bind(this)
	};

	init.apply(this);
	return this;

	function init() {

	}

	function api_prepare(mem) {
		
		this.MEM.mem = mem.buffer;

	}

	function api_renderAll(name) {

		let view = new DataView(this.MEM.mem);
		let canvas = document.createElement("canvas");
		canvas.width = 1024;
		canvas.height = 512;
		let ctx = canvas.getContext("2d");

		let ofs = 0;
		for(let y = 0; y < canvas.height; y++) {
			for(let x = 0; x < canvas.width; x++) {

				let color = view.getUint16(ofs, true);
				let r = ((color >> 0x00) & 0x1f) << 3;
				let g = ((color >> 0x05) & 0x1f) << 3;
				let b = ((color >> 0x0a) & 0x1f) << 3;
				let a = color > 0 ? 1 : 0;
				ctx.fillStyle = "rgba(" + r + "," + g + "," + b + "," + a + ")";
				ctx.fillRect(x, y, 1, 1);
				ofs += 2;

			}

		}

		canvas.toBlob(function(blob) {
			saveAs(blob, name + ".png");
		});

	}

	function api_renderTexture(dword) {
		
		let view = new DataView(this.MEM.mem);
		let canvas = document.createElement("canvas");
		canvas.width = 256;
		canvas.height = 256;
		let ctx = canvas.getContext('2d');

		let image_coords = dword & 0xffff;
		let pallet_coords = dword >> 16;

		let image_x = (image_coords & 0x0f) << 6;
		let image_y = image_coords & 0x10 ? 256 : 0;

		let pallet_x = (pallet_coords & 0x3f) << 4;
		let pallet_y = pallet_coords >> 6;

		// Read the palettes

		let ofs = pallet_y * 1024 * 2;
		ofs += pallet_x * 2;

		let palette = new Array(16);
		for(let i = 0; i < 16; i++) {

			let color = view.getUint16(ofs, true);
			let r = ((color >> 0x00) & 0x1f) << 3;
			let g = ((color >> 0x05) & 0x1f) << 3;
			let b = ((color >> 0x0a) & 0x1f) << 3;
			let a = color > 0 ? 1 : 0;
			palette[i] = "rgba(" + r + "," + g + "," + b + "," + a + ")";
			ofs += 2;

		}

		// Read the body

		for(let y = 0; y < 256; y++) {

			ofs = (image_y+y) * 1024 * 2;
			ofs += image_x * 2;
			
			for(let x = 0; x < 256; x+=2) {
				
				let px = view.getUint8(ofs);
				ofs++
				
				let low = palette[px & 0x0f];
				ctx.fillStyle = low;
				ctx.fillRect(x, y, 1, 1);
				
				let high = palette[px >> 4];
				ctx.fillStyle = high;
				ctx.fillRect(x + 1, y, 1, 1);

			}
			

		}

		return canvas;
	}

}).apply({});
